/* add-on script */
var JWT_TOKEN;

$(document).ready(function () {
    JWT_TOKEN = $('#token').attr('content');

    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var projectKey = getParameterByName("projectKey");

    $(document.body).on('click', "#store-contacts", function (event) {
        var name = $('#name').val();
        var email = $('#email').val();
        var organisation = $('#organisation').val();
        console.log("Project key: '" + projectKey + "'");
        $.ajax({
                type: 'POST',
                url: 'update-contact-information/' + projectKey, 
                contentType: 'application/json',
                data: JSON.stringify({value: {name, email, organisation}}),
            })
            .done(() => {
                var message = AP.messages.info('Done', 'Contacts were successfully updated');
                setTimeout(function () {
                    AP.messages.clear(message);
                }, 2000);
            })
            .fail(() => {
                AP.messages.error('Error', 'There was an error when trying to store contacts');
            });
        return false;
    });

    $.ajaxSetup({
        headers: {'Authorization': "JWT " + JWT_TOKEN}
    });

    $(document).ajaxSuccess(function (event, xhr, settings) {
        refreshToken(xhr);
    });

    function refreshToken(jqXHR) {
        JWT_TOKEN = jqXHR.getResponseHeader('X-acpt');
    }
});