// for the sake of example simplicity we use in-memory database here
module.exports = function () {
    var contacts = {};
    
    return {
        storeContact(projectKey, contact) {
            contacts[projectKey] = contact;
        },
        
        loadContact(projectKey) {
            return contacts[projectKey];
        }
    }
};