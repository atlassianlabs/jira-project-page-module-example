module.exports = function (app, addon) {

    var contacts = require("./contacts")();
    
    app.get('/', function (req, res) {
        res.format({
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

/*
    app.get('/images/link.svg', function(req, res){
        res.redirect(app.locals.furl( '/images/link.svg'));
    });
*/
    
    app.get('/contacts', addon.authenticate(), function (req, res) {
        let projectKey = req.query.projectKey;
        let contact = contacts.loadContact(projectKey);
        res.render('contacts', {
                title: 'Atlassian Connect',
                projectKey: projectKey || '(Unknown project)',
                contact,
            });
        }
    );

    app.get('/contacts-software', addon.authenticate(), function (req, res) {
        let projectKey = req.query.projectKey;
        let contact = contacts.loadContact(projectKey);
        res.render('contacts-software', {
                title: 'Atlassian Connect',
                projectKey: projectKey || '(Unknown project)',
                contact,
            });
        }
    );
    
    app.post('/update-contact-information/:projectKey', addon.checkValidToken(), function (req, res){
        contacts.storeContact(req.params.projectKey, req.body.value);
        res.send(200);
    });

    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for(var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};
