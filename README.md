# Project Page Connect Demo

A Connect add-on to demonstrate project page module in JIRA. It adds a new page called "Project customer contacts" to a project links tab.

## To start the add-on

- Follow the [instructions](https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html) to get a dev instance
- Install package dependencies with `npm install`
- Follow the [instructions](https://bitbucket.org/atlassian/atlassian-connect-express#markdown-header-automatic-registration) to install the add-on on the dev instance

## To test the feature

Open the *Project customer contacts* page from the project links tab. This page shows settings associated with currently selected project. Click *Store contact* to update these settings.
